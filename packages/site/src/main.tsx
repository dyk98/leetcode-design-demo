import React from 'react'
import ReactDOM from 'react-dom/client'
import { Button, Provider } from 'leetcode-design'
import { CoolButton } from 'leetcode-design-us'
import { CoolInput } from 'leetcode-design-cn'

ReactDOM.createRoot(document.getElementById('root') as HTMLElement).render(
  <React.StrictMode>
    <Provider>
      <div>
        <Button>这是一个通用的Button</Button>
        <CoolButton />
        <CoolInput />
      </div>
    </Provider>
  </React.StrictMode>
)
