import { Button } from 'leetcode-design'

export const CoolButton = (props: any) => {

  return (
      <Button bgColor={'red'} {...props}>This is a US Cool Button!!!</Button>
  )

}

export default CoolButton
