import { Input, InputRightElement, InputGroup } from 'leetcode-design'
import { CoolButton } from '@leetcode-design-us/cool-button'


export const CoolInput = () => {

  return (
      <InputGroup mt={"20px"} w={"500px"}>
        <Input placeholder={"这是一个中站 Input"} />
        <InputRightElement w={"250px"}>
          <CoolButton />
        </InputRightElement>
      </InputGroup>
  )

}

export default CoolInput
