import { Button as ChakraButton } from '@chakra-ui/react'

export const Button = (props: any) => {
  return (
      <ChakraButton {...props} />
  )
}

export default Button
