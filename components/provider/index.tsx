import React from 'react'
import { ChakraProvider, extendTheme } from '@chakra-ui/react'
import defaultTheme from '../theme/default'
import cnTheme from '../theme/cn'
import usTheme from '../theme/us'

const themeHash = {
  cn: cnTheme,
  us: usTheme
}

export const Provider = (props: {
  theme?: 'cn' | 'us'
  children: React.ReactNode
}) => {

  const theme = extendTheme(( props.theme ? themeHash?.[props.theme] || defaultTheme : defaultTheme).lightTheme)

  return (
      <ChakraProvider theme={theme}>
        {props.children}
      </ChakraProvider>
  )
}

export default Provider
