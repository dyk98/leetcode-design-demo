import { InputRightElement as ChakraInputRightElement } from '@chakra-ui/react'

export const InputRightElement = (props: any) => {
  return (
      <ChakraInputRightElement {...props} />
  )
}

export default InputRightElement
