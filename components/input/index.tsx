import { Input as ChakraInput } from '@chakra-ui/react'

export const Input = (props) => {
  return (
      <ChakraInput {...props} />
  )
}

export default Input
