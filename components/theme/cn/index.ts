import { buttonTheme } from "./button";

const light = {
  colors: {
    brand: {
      100: "#f7fafc",
      900: "#1a202c",
    },
  },
  components: {
    Button: buttonTheme
  }
}

const dark = {
  components: {
    Button: {
      ...buttonTheme,
      defaultProps: {
        bgColor: 'white',
        color: 'black',
        colorScheme: 'green', // default is gray
      },
    }
  }
}

export default {
  lightTheme: light,
  darkTheme: dark
}
