export const buttonTheme = {
  baseStyle: {
    fontWeight: 'bold',
  },
  sizes: {
    xl: {
      h: '56px',
      fontSize: 'lg',
      px: '32px',
    },
  },
}
