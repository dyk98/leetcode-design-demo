import { InputGroup as ChakraInputGroup } from '@chakra-ui/react'

export const InputGroup = (props) => {
  return (
      <ChakraInputGroup {...props} />
  )
}

export default InputGroup
